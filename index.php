<?php 


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: *');



require 'vendor/autoload.php';


// instantiate the App object
$app = new \Slim\App();

function getDB(){
	$dbhost = "localhost";
	$dbname = "teknol";
	$dbuser = "root";
    $dbpass = "";

    try {
        $conn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully"; 
        }
    catch(PDOException $e)
        {
        echo "Connection failed: " . $e->getMessage();
        }
     
        return $conn;
    
    }
    
    

    //Obtener datos
    $app->get('/all', function ($request, $response, $args) {
        try {

            $db= getDB(); 
            $sth = $db->prepare("SELECT id, ubicacion, tecnico, modelo, servicio, fecha FROM tbl_registros WHERE estado = 1");
        
            $sth->execute();
            $registros = $sth->fetchAll(PDO::FETCH_ASSOC);
        
            if ($registros) {
                $response=$response->withJson($registros);
                $db=null;
            }
        
        } catch (PDOException $e) {
            $response->write('{"estado":'.$e->getMessage().'}');
        }
           return $response; 
        });

    //obtener uno
    $app->get('/one/{id}', function ($request, $response, $args) {
        try {

            $db= getDB(); 
            $sth = $db->prepare("SELECT * FROM tbl_registros WHERE id = :id");
            $sth->bindParam(":id", $args["id"], PDO::PARAM_INT);
            $sth->execute();
            $registros = $sth->fetchAll(PDO::FETCH_ASSOC);
        
            if ($registros) {
                $response=$response->withJson($registros);
                $db=null;
            }
        
        } catch (PDOException $e) {
            $response->write('{"estado":'.$e->getMessage().'}');
        }
           return $response; 
        });

    

    //Agregar nuevo Registro
        $app->post('/add', function($request, $response){
   
            try {
        
                $data = $request->getParams();
                $db   = getDB(); 
                $sth  = $db->prepare("INSERT INTO tbl_registros
                    (ubicacion, tecnico, modelo, servicio, fecha)
                    VALUES(?,?,?,?,?)
                    ");
                $sth->execute(array( $data["ubicacion"], $data["tecnico"], $data["modelo"], $data["servicio"], $data["fecha"] ));
                
                $response->write('{"estado":"1"}');
        
            } catch (PDOException $e) {
                //$response->write('error de algo');
                $response->write('{"estado":'.$e->getMessage().'}');
            }
        
        return $response;
        
        });
        

        //Modificar registros

        $app->put('/update', function ($request, $response, $args) {
            try {

                $data = $request->getParams();
                $db= getDB(); 
                $sth = $db->prepare("UPDATE tbl_registros SET ubicacion=?, tecnico=?, modelo=?, servicio=? WHERE id=?");
            
                $sth->execute(array($data["ubicacion"], $data["tecnico"], $data["modelo"], $data["servicio"], $data["id"]));
                $response->write('{"error":"ok"}');
            
            }catch(PDOException $e){
                $response->write('{"error":{"texto":'.$e->getMessage().'}}');
            }
               return $response; 
            });



    //Eliminar registros
     $app->delete('/delete/{id}', function(  $request,  $response,  $args){
            
       try {
    
            $db   = getDB();
            $sth  = $db->prepare("DELETE FROM tbl_registros WHERE id=:id");
           // $sth = $db->prepare("UPDATE tbl_registros SET estado=0 WHERE id=?");
            $sth->bindParam(":id", $args["id"], PDO::PARAM_INT);
            $sth->execute();
            $response->write('{"error":"ok"}');
            
       } catch (PDOException $e) {
            $response->write('{"error:{"texto":'.$e->getMessage().'}}');
       }
        
    return $response;
    
    }); 

/*
     $app->put('/delete/{id}', function ($request, $response, $args) {
        try {

            $data = $request->getParams();
            $db= getDB(); 
            $sth = $db->prepare("UPDATE tbl_registros SET estado=0 WHERE id=?");
        
            $sth->execute(array($data["ubicacion"], $data["tecnico"], $data["modelo"], $data["servicio"], $data["id"]));
            $response->write('{"error":"ok"}');
        
        }catch(PDOException $e){
            $response->write('{"error":{"texto":'.$e->getMessage().'}}');
        }
           return $response; 
        });
*/
    


        $app->run();

 ?>