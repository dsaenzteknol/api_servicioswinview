-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2018 a las 21:11:07
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `teknol`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_registros`
--

CREATE TABLE `tbl_registros` (
  `id` int(11) NOT NULL,
  `ubicacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tecnico` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `servicio` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitud` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `estado` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_registros`
--

INSERT INTO `tbl_registros` (`id`, `ubicacion`, `tecnico`, `modelo`, `servicio`, `longitud`, `latitud`, `fecha`, `estado`) VALUES
(4, 'Casa4', 'Francisco', 'Fco123', 'qweqwe', '-8.7592716', '42.2023782', '2018-05-26', 1),
(6, 'Dominos P', 'Fco Hdez', 'tkn3', 'Pantalla Rota', '-8.7683697', '42.2011860', '2018-05-26', 1),
(7, 'Gallerias', 'Francisco', 'tkn28', 'Desenchifado', '', '', '2018-05-25', 1),
(8, 'Yo Robot 1', 'Rob', 'oT', 'Robot1', '', '', '2018-05-28', 1),
(13, 'Tekkkk', 'qwe', 'qwe', 'qwe', '', '', '2018-06-05', 1),
(14, 'Wairo', 'qwe', 'qwe', 'qwe', '', '', '2018-06-05', 1),
(15, 'Ubicacion', 'Tecnico', 'Modeo', 'Servicio', '', '', '2018-06-05', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_registros`
--
ALTER TABLE `tbl_registros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_registros`
--
ALTER TABLE `tbl_registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
